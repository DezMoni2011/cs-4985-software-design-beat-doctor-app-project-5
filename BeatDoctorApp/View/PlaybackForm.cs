﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BeatDoctorApp.Model;

namespace BeatDoctorApp.View
{
    public partial class PlaybackForm : Form
    {
        private Sound sound;
        private bool playing;
        
        public PlaybackForm()
        {
            InitializeComponent();

            uint currentVolume = 0;
            this.sound = new Sound(Convert.ToInt32(this.tempoBPMNumericUpDown.Value));

            Sound.waveOutGetVolume(IntPtr.Zero, out currentVolume);
            ushort calcVolume = (ushort) (currentVolume & 0x0000ffff);
            this.volumeTrackBar.Value = calcVolume/(ushort.MaxValue/100);

            this.volumeTrackBar.Value = 100;
            var defaultTimeSignature = new TimeSignature(4,4);
            this.populateWith(this.timeSignatureComboBox, new TimeSignature(2,4), new TimeSignature(3, 4), defaultTimeSignature, new TimeSignature(6,8), new TimeSignature(9, 4), new TimeSignature(12,8) );
            this.timeSignatureComboBox.SelectedItem = defaultTimeSignature;

            this.populateWith(this.tempoComboBox, Tempo.GetTempoNames());
            this.tempoBPMNumericUpDown.Value = 143;

            this.pauseButton.Enabled = false;
            this.stopButton.Enabled = false;
            this.soundTickBox1 = new SoundTickBox(this.timeSignatureComboBox.SelectedItem as TimeSignature, 143, this.label1);
            this.soundTickBox1.UpdateInterval();
        }

        private void populateWith(ComboBox itemsHolder, params TimeSignature[] objects)
        {
            foreach (var word in objects)
            {
                itemsHolder.Items.Add(word);
            }
        }

        private void populateWith(ComboBox itemsHolder, Dictionary<string, Tuple<int, int>>.KeyCollection set)
        {
            foreach (var item in set)
            {
                itemsHolder.Items.Add(item);
            }
        }
 
        private void playButton_Click(object sender, EventArgs e)
        {

            this.editButton.Enabled = false;
            this.playButton.Enabled = false;
            this.pauseButton.Enabled = true;
            this.stopButton.Enabled = true;

            this.playing = true;
            this.soundTickBox1.Start();
            
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            this.playing = false;

            this.editButton.Enabled = true;
            this.playButton.Enabled = true;
            this.pauseButton.Enabled = false;
            this.stopButton.Enabled = true;

            this.sound.StopMetronomeSound();
            this.soundTickBox1.Stop();
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            this.playing = false;

            this.editButton.Enabled = true;
            this.playButton.Enabled = true;
            this.pauseButton.Enabled = false;
            this.stopButton.Enabled = true;

            this.sound.StopMetronomeSound();
            this.soundTickBox1.Stop();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            toggleButtonTextAndAction();
        }

        private void toggleButtonTextAndAction()
        {
            if (this.editButton.Text.Equals("Edit"))
            {
                this.editButton.Text = "Set";
                this.showEditableControls();
                this.disableAllPlaybackButtons();
                
            }
            else
            {
                this.editButton.Text = "Edit";
                this.hideEditableControls();
                this.enableAllPlaybackButtons();
            }


            // TODO : Need to handle enabling for scenario when user selects play | pauae or stop | edit | set
        }

        private void showEditableControls()
        {
            this.setVisibilityForEditableAndNoneditableControls(true);
        }

        private void hideEditableControls()
        {
            this.setVisibilityForEditableAndNoneditableControls(false);
        }

        private void setVisibilityForEditableAndNoneditableControls(bool visible)
        {
            this.tempoBPMNumericUpDown.Visible = visible;
            this.timeSignatureComboBox.Visible = visible;
            this.tempoComboBox.Visible = visible;
            this.playbackTimeSignatureLabel.Visible = !visible;
            this.playbackTempoLabel.Visible = !visible;
        }

        private void disableAllPlaybackButtons()
        {
            this.setPlaybackButtonsEnabling(false);
        }

        private void enableAllPlaybackButtons()
        {
            this.setPlaybackButtonsEnabling(true);
        }

        private void setPlaybackButtonsEnabling(bool enabled)
        {
            this.playButton.Enabled = enabled;
            this.pauseButton.Enabled = enabled;
            this.stopButton.Enabled = enabled;
        }
        

        private void volumeTrackBar_ValueChanged(object sender, EventArgs e)
        {
            this.bindViewToVolumeChanges();
        }

        private void bindViewToVolumeChanges()
        {
            this.volumeValueLabel.Text = this.volumeTrackBar.Value.ToString();
        }

        private void tempoBPMNumericUpDown_ValueChanged(object sender, EventArgs e)
        {
            this.bindTempoComboBoxToTempoNumericUpDownChangedValue();
            this.bindViewToBPMChanges();
            this.soundTickBox1.BPM = (int) this.tempoBPMNumericUpDown.Value;
            this.soundTickBox1.UpdateInterval();
        }

        private void bindTempoComboBoxToTempoNumericUpDownChangedValue()
        {
            this.tempoComboBox.SelectedItem = Tempo.GetNameForTempoAt((int) this.tempoBPMNumericUpDown.Value);
        }

        private void bindViewToBPMChanges()
        {
            this.playbackTempoLabel.Text = string.Format("{0} BPM {1}", this.tempoBPMNumericUpDown.Value,
                this.tempoComboBox.SelectedItem);
        }

        private void tempoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bindTempoNumericUpDownValueToTempoComboBoxSelection();
            this.bindViewToBPMChanges();
            this.soundTickBox1.BPM = (int)this.tempoBPMNumericUpDown.Value;
            this.soundTickBox1.UpdateInterval();
        }

        private void bindTempoNumericUpDownValueToTempoComboBoxSelection()
        {
            var tempo = Tempo.GetBPMForTempo(this.tempoComboBox.SelectedItem as string);

            if (tempo < this.tempoBPMNumericUpDown.Minimum)
            {
                this.tempoBPMNumericUpDown.Value = this.tempoBPMNumericUpDown.Minimum;
            }
            else if (tempo > this.tempoBPMNumericUpDown.Maximum)
            {
                this.tempoBPMNumericUpDown.Value = this.tempoBPMNumericUpDown.Maximum;
            }
            else
            {
                this.tempoBPMNumericUpDown.Value = tempo;
            }
        }

        private void timeSignatureComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.bindViewToTimeSignatureChanges();
            this.soundTickBox1.TimeSignature = this.timeSignatureComboBox.SelectedItem as TimeSignature;
        }

        private void bindViewToTimeSignatureChanges()
        {
            this.playbackTimeSignatureLabel.Text = this.timeSignatureComboBox.SelectedItem.ToString();
        }

        private void volumeTrackBar_Scroll(object sender, EventArgs e)
        {
            int newVolume = ((ushort.MaxValue/10)*this.volumeTrackBar.Value);
            uint newVolumeAllChannels = ((uint) newVolume & 0x0000ffff) | ((uint) newVolume << 20);
            Sound.waveOutSetVolume(IntPtr.Zero, newVolumeAllChannels);
        }
    }
}
