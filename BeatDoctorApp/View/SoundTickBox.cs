﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using BeatDoctorApp.Model;
using BeatDoctorApp.Properties;

namespace BeatDoctorApp.View
{
    public partial class SoundTickBox : UserControl
    {
        public int BPM { get; set; }
        private System.Timers.Timer tickTimer;
        private int beatCount;
 
        public SoundTickBox()
        {
            InitializeComponent();
            this.BPM = 60;
            this.Label = null;
            this.tickTimer = new System.Timers.Timer();
            this.tickTimer.Elapsed += tickTimer_Tick;
            this.TimeSignature = new TimeSignature(4,4);
            this.beatCount = 0;

        }

        public TimeSignature TimeSignature { get; set; }

        public SoundTickBox(int bpm) : this()
        {
            this.BPM = bpm;
        }

        public void UpdateInterval()
        {
            int invariant = (60000 / this.BPM);
            this.tickTimer.Interval = invariant;
        }

        public SoundTickBox(Label label) : this()
        {
            this.Label = label;
        }

        public SoundTickBox(TimeSignature signature, int bpm, Label display) : this(bpm)
        {
            this.Label = display;
            this.TimeSignature = signature;
        }

        public Label Label { get;}


        private void Flash(Color flashColor)
        {
            this.BackColor = flashColor;
            this.Label.ForeColor = flashColor;
            this.Label.BackColor = flashColor;


        }

        public void Start()
        {
            this.Label.Show();
            
            tickTimer.Start();
        }

        public void Stop()
        {
            this.Label.Hide();
            this.tickTimer.Stop();
            this.Flash(Color.Tomato);
            this.beatCount = 0;


        }

        private void tickTimer_Tick(object sender, ElapsedEventArgs e)
        {
            
            this.Flash(Color.DarkCyan);
            if (beatCount%this.TimeSignature.TopNumber == 0)
            {
                Console.Beep(10000, 75);
            }
            else
            {
                Console.Beep(4000, 75);
            }

            this.Flash(Color.White);
            this.beatCount++;

        }
    }
            
      
}
