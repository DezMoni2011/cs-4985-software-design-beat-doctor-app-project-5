﻿namespace BeatDoctorApp.View
{
    partial class PlaybackForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.stopButton = new System.Windows.Forms.Button();
            this.volumeLabel = new System.Windows.Forms.Label();
            this.timeSignatureLabel = new System.Windows.Forms.Label();
            this.playbackTimeSignatureLabel = new System.Windows.Forms.Label();
            this.tempoLabel = new System.Windows.Forms.Label();
            this.playbackTempoLabel = new System.Windows.Forms.Label();
            this.volumeTrackBar = new System.Windows.Forms.TrackBar();
            this.timeSignatureComboBox = new System.Windows.Forms.ComboBox();
            this.tempoComboBox = new System.Windows.Forms.ComboBox();
            this.tempoBPMNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.volumeValueLabel = new System.Windows.Forms.Label();
            this.soundTickBox1 = new BeatDoctorApp.View.SoundTickBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempoBPMNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(332, 183);
            this.editButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(100, 28);
            this.editButton.TabIndex = 0;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // playButton
            // 
            this.playButton.Image = global::BeatDoctorApp.Properties.Resources.play;
            this.playButton.Location = new System.Drawing.Point(69, 290);
            this.playButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(167, 71);
            this.playButton.TabIndex = 1;
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Image = global::BeatDoctorApp.Properties.Resources.pause;
            this.pauseButton.Location = new System.Drawing.Point(304, 290);
            this.pauseButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(167, 71);
            this.pauseButton.TabIndex = 2;
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.BackColor = System.Drawing.Color.Transparent;
            this.stopButton.Image = global::BeatDoctorApp.Properties.Resources.stop;
            this.stopButton.Location = new System.Drawing.Point(529, 290);
            this.stopButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(167, 71);
            this.stopButton.TabIndex = 3;
            this.stopButton.UseVisualStyleBackColor = false;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // volumeLabel
            // 
            this.volumeLabel.AutoSize = true;
            this.volumeLabel.Location = new System.Drawing.Point(761, 11);
            this.volumeLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.volumeLabel.Name = "volumeLabel";
            this.volumeLabel.Size = new System.Drawing.Size(55, 17);
            this.volumeLabel.TabIndex = 4;
            this.volumeLabel.Text = "Volume";
            // 
            // timeSignatureLabel
            // 
            this.timeSignatureLabel.AutoSize = true;
            this.timeSignatureLabel.Location = new System.Drawing.Point(189, 47);
            this.timeSignatureLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.timeSignatureLabel.Name = "timeSignatureLabel";
            this.timeSignatureLabel.Size = new System.Drawing.Size(112, 17);
            this.timeSignatureLabel.TabIndex = 5;
            this.timeSignatureLabel.Text = "Time Signature :";
            // 
            // playbackTimeSignatureLabel
            // 
            this.playbackTimeSignatureLabel.AutoSize = true;
            this.playbackTimeSignatureLabel.Location = new System.Drawing.Point(328, 47);
            this.playbackTimeSignatureLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.playbackTimeSignatureLabel.Name = "playbackTimeSignatureLabel";
            this.playbackTimeSignatureLabel.Size = new System.Drawing.Size(71, 17);
            this.playbackTimeSignatureLabel.TabIndex = 6;
            this.playbackTimeSignatureLabel.Text = "TIME_SIG";
            // 
            // tempoLabel
            // 
            this.tempoLabel.AutoSize = true;
            this.tempoLabel.Location = new System.Drawing.Point(240, 119);
            this.tempoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.tempoLabel.Name = "tempoLabel";
            this.tempoLabel.Size = new System.Drawing.Size(60, 17);
            this.tempoLabel.TabIndex = 7;
            this.tempoLabel.Text = "Tempo :";
            // 
            // playbackTempoLabel
            // 
            this.playbackTempoLabel.AutoSize = true;
            this.playbackTempoLabel.Location = new System.Drawing.Point(328, 119);
            this.playbackTempoLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.playbackTempoLabel.Name = "playbackTempoLabel";
            this.playbackTempoLabel.Size = new System.Drawing.Size(169, 17);
            this.playbackTempoLabel.TabIndex = 8;
            this.playbackTempoLabel.Text = "TEMPO_NAME    TEMPO";
            // 
            // volumeTrackBar
            // 
            this.volumeTrackBar.Location = new System.Drawing.Point(765, 66);
            this.volumeTrackBar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.volumeTrackBar.Maximum = 100;
            this.volumeTrackBar.Name = "volumeTrackBar";
            this.volumeTrackBar.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.volumeTrackBar.Size = new System.Drawing.Size(56, 417);
            this.volumeTrackBar.SmallChange = 5;
            this.volumeTrackBar.TabIndex = 9;
            this.volumeTrackBar.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.volumeTrackBar.Scroll += new System.EventHandler(this.volumeTrackBar_Scroll);
            this.volumeTrackBar.ValueChanged += new System.EventHandler(this.volumeTrackBar_ValueChanged);
            // 
            // timeSignatureComboBox
            // 
            this.timeSignatureComboBox.FormattingEnabled = true;
            this.timeSignatureComboBox.Location = new System.Drawing.Point(332, 43);
            this.timeSignatureComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.timeSignatureComboBox.Name = "timeSignatureComboBox";
            this.timeSignatureComboBox.Size = new System.Drawing.Size(195, 24);
            this.timeSignatureComboBox.TabIndex = 10;
            this.timeSignatureComboBox.Text = "Select Time Signature";
            this.timeSignatureComboBox.Visible = false;
            this.timeSignatureComboBox.SelectedIndexChanged += new System.EventHandler(this.timeSignatureComboBox_SelectedIndexChanged);
            // 
            // tempoComboBox
            // 
            this.tempoComboBox.FormattingEnabled = true;
            this.tempoComboBox.Location = new System.Drawing.Point(449, 116);
            this.tempoComboBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tempoComboBox.Name = "tempoComboBox";
            this.tempoComboBox.Size = new System.Drawing.Size(195, 24);
            this.tempoComboBox.TabIndex = 11;
            this.tempoComboBox.Text = "Select Tempo";
            this.tempoComboBox.Visible = false;
            this.tempoComboBox.SelectedIndexChanged += new System.EventHandler(this.tempoComboBox_SelectedIndexChanged);
            // 
            // tempoBPMNumericUpDown
            // 
            this.tempoBPMNumericUpDown.Location = new System.Drawing.Point(332, 117);
            this.tempoBPMNumericUpDown.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tempoBPMNumericUpDown.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.tempoBPMNumericUpDown.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.tempoBPMNumericUpDown.Name = "tempoBPMNumericUpDown";
            this.tempoBPMNumericUpDown.Size = new System.Drawing.Size(81, 22);
            this.tempoBPMNumericUpDown.TabIndex = 12;
            this.tempoBPMNumericUpDown.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.tempoBPMNumericUpDown.Visible = false;
            this.tempoBPMNumericUpDown.ValueChanged += new System.EventHandler(this.tempoBPMNumericUpDown_ValueChanged);
            // 
            // volumeValueLabel
            // 
            this.volumeValueLabel.AutoSize = true;
            this.volumeValueLabel.Location = new System.Drawing.Point(728, 47);
            this.volumeValueLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.volumeValueLabel.Name = "volumeValueLabel";
            this.volumeValueLabel.Size = new System.Drawing.Size(119, 17);
            this.volumeValueLabel.TabIndex = 13;
            this.volumeValueLabel.Text = "VOLUME_VALUE";
            // 
            // soundTickBox1
            // 
            this.soundTickBox1.BPM = 60;
            this.soundTickBox1.Location = new System.Drawing.Point(-1, -2);
            this.soundTickBox1.Margin = new System.Windows.Forms.Padding(4);
            this.soundTickBox1.Name = "soundTickBox1";
            this.soundTickBox1.Size = new System.Drawing.Size(123, 138);
            this.soundTickBox1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(325, 385);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 135);
            this.label1.TabIndex = 15;
            this.label1.Text = "d";
            // 
            // PlaybackForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(856, 529);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.soundTickBox1);
            this.Controls.Add(this.volumeValueLabel);
            this.Controls.Add(this.tempoBPMNumericUpDown);
            this.Controls.Add(this.tempoComboBox);
            this.Controls.Add(this.timeSignatureComboBox);
            this.Controls.Add(this.volumeTrackBar);
            this.Controls.Add(this.playbackTempoLabel);
            this.Controls.Add(this.tempoLabel);
            this.Controls.Add(this.playbackTimeSignatureLabel);
            this.Controls.Add(this.timeSignatureLabel);
            this.Controls.Add(this.volumeLabel);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.playButton);
            this.Controls.Add(this.editButton);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "PlaybackForm";
            this.Text = "Beat Doctor By KDog & DMoni";
            ((System.ComponentModel.ISupportInitialize)(this.volumeTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tempoBPMNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Label volumeLabel;
        private System.Windows.Forms.Label timeSignatureLabel;
        private System.Windows.Forms.Label playbackTimeSignatureLabel;
        private System.Windows.Forms.Label tempoLabel;
        private System.Windows.Forms.Label playbackTempoLabel;
        private System.Windows.Forms.TrackBar volumeTrackBar;
        private System.Windows.Forms.ComboBox timeSignatureComboBox;
        private System.Windows.Forms.ComboBox tempoComboBox;
        private System.Windows.Forms.Label volumeValueLabel;
        public System.Windows.Forms.NumericUpDown tempoBPMNumericUpDown;
        public System.Windows.Forms.Button playButton;
        private View.SoundTickBox soundTickBox1;
        private System.Windows.Forms.Label label1;
    }
}

