﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeatDoctorApp.Model.Interfaces
{
    public enum Action
    {
        PLAY,
        PAUSE,
        STOP
    }

    //TODO: Document class specs
    public interface IReceiver
    {

        //TODO: Document method specs
        void SetAction(Action action);

        //TODO: Document method specs
        int GetResults();

    }
}
