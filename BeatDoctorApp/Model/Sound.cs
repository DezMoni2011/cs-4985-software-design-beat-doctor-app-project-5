﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BeatDoctorApp.Properties;

namespace BeatDoctorApp.Model
{
    public class Sound
    {
        private const int AppcommandVolumeMute = 0x80000;
        private const int AppcommandVolumeUp = 0XA0000;
        private const int AppcommandVolumeDown = 0x90000;
        private const int WmAppcommand = 0x319;

        private readonly IntPtr handle = Process.GetCurrentProcess().MainWindowHandle;
        private System.Media.SoundPlayer player;
        private int bpmValue;

        public Sound(int numUpDown)
        {
            this.bpmValue = numUpDown/60;
        }

        /// <summary>
        ///     Plays the metronome sound for each beat of the BPM. Used with the Play button.
        ///     Precondition: None
        ///     Postcondition: The metronome sound is played
        /// </summary>
        public void PlayMetronomeSound()
        {
            //System.Media.SystemSounds.Beep.Play();
            this.player =
                new System.Media.SoundPlayer(Resources.woodblock);
            this.player.Load();
            this.player.Play();
        }


        public void StopMetronomeSound()
        {
            this.player?.Stop();
        }

        [DllImport("winmm.dll")]
        public static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);

        [DllImport("winmm.dll")]
        public static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);
    }
}
