﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeatDoctorApp.Model
{
    //TODO: Document class specs
    public static class Tempo
    {
        private static Dictionary<string, Tuple<int, int>> tempos = new Dictionary<string, Tuple<int, int>>
        {
            { "Larghissimo" , Tuple.Create(0, 24)},
            { "Grave" , Tuple.Create(25, 39)},
            { "Largo" , Tuple.Create(40, 44)},
            { "Lento" , Tuple.Create(45, 59)},
            { "Larghetto" , Tuple.Create(60, 65)},
            { "Adagio" , Tuple.Create(66, 71)},
            { "Adagietto" , Tuple.Create(72, 75)},
            { "Andante" , Tuple.Create(76, 79)},
            { "Andantino" , Tuple.Create(80, 82)},
            { "Marcia Moderato" , Tuple.Create(83, 91)},
            { "Andante Moderato" , Tuple.Create(92, 107)},
            { "Moderato " , Tuple.Create(108, 111)},
            { "Allegretto" , Tuple.Create(112, 115)},
            { "Allegro Moderato" , Tuple.Create(116, 119)},
            { "Allegro" , Tuple.Create(120, 167)},
            { "Vivace" , Tuple.Create(168, 171)},
            { "Allegro Vivace" , Tuple.Create(172, 176)},
            { "Presto" , Tuple.Create(168, 199)},
            { "Prestissimo" , Tuple.Create(200, 250)}
        };

        //TODO: Document method specs
        public static string GetNameForTempoAt(int tempo)
        {
            foreach (var tempoItem in tempos)
            {
                var tempoName = tempoItem.Key;
                int min = tempoItem.Value.Item1;
                int max = tempoItem.Value.Item2;

                if (tempo >= min && tempo <= max)
                {
                    return tempoName;

                }
            }
            return null;
        }

        //TODO: Document method specs
        public static Dictionary<string, Tuple<int, int>>.KeyCollection GetTempoNames()
        {
            return tempos.Keys;
        }

        //TODO: Document method specs
        public static int GetBPMForTempo(string tempo)
        {
            if (!tempos.ContainsKey(tempo))
            {
                return Int32.MinValue;
            }

            var numberOfOptions = tempos[tempo].Item2 - tempos[tempo].Item1;
            var bpm = tempos[tempo].Item1;

            var options = new int[numberOfOptions];
            for (int i = 0; i < numberOfOptions; i++)
            {
                options[i] = bpm;
                bpm++;
            }

            if (numberOfOptions%2 == 0)
            {
                return options[numberOfOptions/ 2];
            }

            return options[(options.Length - 1)/2];
        }

        //TODO: Implememnt method GetMinFor(tempoName) : int

        //TODO: Implememnt method GetMaxFor(tempoName) : int
    }
}
