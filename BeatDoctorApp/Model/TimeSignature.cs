﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeatDoctorApp.Model
{
    //TODO: Document class specs
    public class TimeSignature
    {
        //TODO: Document property specs
        public int TopNumber { get; }

        //TODO: Document property specs
        public int BottomNumber { get; }

        //TODO: Document constructor specs
        public TimeSignature(int numberOfBeats, int valueOfOneBeat)
        {
            //TODO: Perform Exception Checking Here.
            this.TopNumber = numberOfBeats;
            this.BottomNumber = valueOfOneBeat;
        }

        //TODO: Document method specs
        public override string ToString()
        {
            return string.Format("{0}/{1}", this.TopNumber, this.BottomNumber);
        }
    }
}
